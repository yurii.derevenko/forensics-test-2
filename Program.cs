﻿using System;
using System.Collections.ObjectModel;

using Animals;
using AnimalsCollection;
using DefineIDangerous;
using SpecificAnimals;

namespace secondAssessment
{
    class Program
    {
        static void Main(string[] args)
        {
          ObservableCollection<Animal> animals = new AnimalCollection();
            
          var doggy = new Dog("Doggy", "picture1");
          var wolfy = new Wolf("Wolfy", "picture2");
          var dolph = new Dolphin("Dolph", "picture3");
          var babyShark = new Shark("BabyShark", "picture4");
          var itsyBitsy = new Tarantula("ItsyBitsy", "picture4");
          
          animals.Add(doggy);
          animals.Add(wolfy);
          animals.Add(dolph);
          animals.Add(babyShark);
          animals.Add(itsyBitsy);

          // I have not find good enough solution to combine
          // lambda expressions and ObservableCollection

           foreach (Animal a in animals) {
            if (a is IDangerous) {
              Console.WriteLine(a.Name);
            }
          } 
         
          Console.WriteLine("End>>>>>>>");
        }
    }
}
