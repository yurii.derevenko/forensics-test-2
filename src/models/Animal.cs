using System;

namespace Animals
{
  public class Animal
  {
    public string Name { get; }
    public string Picture { get; }

    public Animal(string name, string picture)
    {
      Name = name;
      Picture = picture;
    }
  }

  static class AnimalExtensions
  {
    public static void Feed(this Animal animal) {
    }
  }
}
