

namespace SpecificAnimals
{
  using System;
  using Animals;
  using DefineMammalKinds;
  using DefineAnimalKinds;
  using DefineIDangerous;
 

  public class Tarantula : Animal, IInsect, IDangerous
  {
    public Tarantula(string name, string picture) : base(name, picture)
    {
      
    }
  }

  public class Shark : Animal, IFish, IDangerous
  {
    public Shark(string name, string picture) : base(name, picture)
    {
      
    }
  }

  public class Dolphin : Animal, IMarineAnimal
  {
    public Dolphin(string name, string picture) : base(name, picture)
    {
      
    }
  }

  public class Wolf: Animal, IPredators, IDangerous
  {
    public Wolf(string name, string picture) : base(name, picture)
    {
      
    }
  }

  public class Dog: Animal, IPet
  {
    public Dog(string name, string picture) : base(name, picture)
    {
      
    }
  }
}