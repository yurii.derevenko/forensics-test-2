namespace DefineMammalKinds
{
  using System;
  using DefineAnimalKinds;

  public interface IPredators : IMammal
  {
  }

  public interface IHerdAnimal : IMammal
  {
  }

  public interface IMarineAnimal : IMammal
  {
  }

  public interface IPet : IMammal
  {
  }
}